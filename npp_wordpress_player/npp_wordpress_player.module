<?php
/**
 * @file
 * Adds a formatter for the npr_audio field.
 */

/**
 * Implements hook_field_formatter_info().
 */
function npp_wordpress_player_field_formatter_info() {
  return array(
    'npp_wordpress_player_formatter' => array(
      'label' => t('NPP Wordpress Player'),
      'field types' => array('npr_audio'),
      'settings' => array(
        'wordpress_player_height' => '24',
        'wordpress_player_width' => '290',
        'wordpress_player_location' => '/sites/all/libraries/player/audio-player',
        'wordpress_player_class' => 'audioplayer2',
        'wordpress_player_title_tag' => 'Playing...',
        'wordpress_player_element_color_bg' => 'CDDFF3',
        'wordpress_player_element_color_leftbg' => '357DCE',
        'wordpress_player_element_color_lefticon' => 'F2F2F2',
        'wordpress_player_element_color_rightbg' => 'F06A51',
        'wordpress_player_element_color_rightbghover' => 'AF2910',
        'wordpress_player_element_color_righticon' => 'F2F2F2',
        'wordpress_player_element_color_righticonhover' => 'FFFFFF',
        'wordpress_player_element_color_text' => '357DCE',
        'wordpress_player_element_color_slider' => '357DCE',
        'wordpress_player_element_color_track' => 'FFFFFF',
        'wordpress_player_element_color_border' => 'FFFFFF',
        'wordpress_player_element_color_loader' => 'AF2910',
        'wordpress_player_download_link' => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function npp_wordpress_player_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();
  $element['wordpress_player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('Set the height of the player (pixels).'),
    '#default_value' => $settings['wordpress_player_height'],
  );
  $element['wordpress_player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('Set the width of the player (pixels).'),
    '#default_value' => $settings['wordpress_player_width'],
  );
  $element['wordpress_player_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Directory'),
    '#description' => t('Absolute local location of the player directory without a trailing slash.'),
    '#default_value' => $settings['wordpress_player_location'],
  );
  $element['wordpress_player_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Class'),
    '#description' => t('List of CSS classes applied to the player element. Separate multiple classes with a space.'),
    '#default_value' => $settings['wordpress_player_class'],
  );
  $element['wordpress_player_title_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Title Tag'),
    '#description' => t('What to display as the title of the audio when the MP3 does not have an ID3 tag. Leave blank to use the MP3 ID tag. Note:
                            if you leave this field blank and the MPR does not have an ID3 tag, "Track #1" will appear as the title.'),
    '#default_value' => $settings['wordpress_player_title_tag'],
  );
  $element['wordpress_player_element_color_bg'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Background Color'),
    '#default_value' => $settings['wordpress_player_element_color_bg'],
  );
  $element['wordpress_player_element_color_leftbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Left Background Color'),
    '#default_value' => $settings['wordpress_player_element_color_leftbg'],
  );
  $element['wordpress_player_element_color_lefticon'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Left Icon Color'),
    '#default_value' => $settings['wordpress_player_element_color_lefticon'],
  );
  $element['wordpress_player_element_color_rightbg'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Right Background Color'),
    '#default_value' => $settings['wordpress_player_element_color_rightbg'],
  );
  $element['wordpress_player_element_color_rightbghover'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Right Background Hover Color'),
    '#default_value' => $settings['wordpress_player_element_color_rightbghover'],
  );
  $element['wordpress_player_element_color_righticon'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Right Icon Color'),
    '#default_value' => $settings['wordpress_player_element_color_righticon'],
  );
  $element['wordpress_player_element_color_righticonhover'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Right Icon Hoever Color'),
    '#default_value' => $settings['wordpress_player_element_color_righticonhover'],
  );
  $element['wordpress_player_element_color_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Text Color'),
    '#default_value' => $settings['wordpress_player_element_color_text'],
  );
  $element['wordpress_player_element_color_slider'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Slider Color'),
    '#default_value' => $settings['wordpress_player_element_color_slider'],
  );
  $element['wordpress_player_element_color_track'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Track Color'),
    '#default_value' => $settings['wordpress_player_element_color_track'],
  );
  $element['wordpress_player_element_color_border'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Border Color'),
    '#default_value' => $settings['wordpress_player_element_color_border'],
  );
  $element['wordpress_player_element_color_loader'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Loader Color'),
    '#default_value' => $settings['wordpress_player_element_color_loader'],
  );
  $element['wordpress_player_download_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Download Link'),
    '#description' => t('Enable/Disable a download link for the audio.'),
    '#default_value' => $settings['wordpress_player_download_link'],
  );
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function npp_wordpress_player_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = t('Configure some player settings like colors and player directory.');
  return $summary;
}

/**
 * Implements hook_theme().
 */
function npp_wordpress_player_theme($exisiting, $type, $theme, $path) {
  $functions = array();
  $functions['wordpress_player'] = array(
    'variables' => array(
      'class' => NULL,
      'height' => NULL,
      'width' => NULL,
      'location' => NULL,
      'bg' => NULL,
      'leftbg' => NULL,
      'lefticon' => NULL,
      'rightbg' => NULL,
      'rightbghover' => NULL,
      'righticon' => NULL,
      'righticonhover' => NULL,
      'text' => NULL,
      'slider' => NULL,
      'track' => NULL,
      'border' => NULL,
      'loader' => NULL,
      'titles' => NULL,
      'mp3' => NULL,
      'downloadlink' => NULL,
    ),
  );

  return $functions;
}

/**
 * Registered theme function that outputs player HTML.
 */
function theme_wordpress_player($args) {
  $class = $args['class'];
  $height = $args['height'];
  $width = $args['width'];
  $location = $args['location'];
  $bg = $args['bg'];
  $leftbg = $args['leftbg'];
  $lefticon = $args['lefticon'];
  $rightbg = $args['rightbg'];
  $rightbghover = $args['rightbghover'];
  $righticon = $args['righticon'];
  $righticonhover = $args['righticonhover'];
  $text = $args['text'];
  $slider = $args['slider'];
  $track = $args['track'];
  $border = $args['border'];
  $loader = $args['loader'];
  $titles = $args['titles'];
  $mp3 = $args['mp3'];

  $html = <<<HTML
    <object class="$class" height="$height" width="$width" data="$location/player.swf" type="application/x-shockwave-flash">
      <param value="$location/player.swf" name="movie">
      <param value="playerID=2&bg=0x$bg&leftbg=0x$leftbg&lefticon=0x$lefticon&rightbg=0x$rightbg&rightbghover=0x$rightbghover&righticon=0x$righticon&righticonhover=0x$righticonhover&text=0x$text&slider=0x$slider&track=0x$track&border=0x$border&loader=0x$loader&titles=$titles&soundFile=$mp3" name="FlashVars">
      <param value="high" name="quality">
      <param value="true" name="menu">
      <param value="transparent" name="wmode">
    </object>
HTML;

  if ($args['downloadlink']) {
    $html .= '<div class="npp_download_link"><a href="' . $mp3 . '">Download this audio</a></div>';
  }

  return $html;
}

/**
 * Implements hook_field_formatter_view().
 */
function npp_wordpress_player_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  // Require the mp3 scraper library.
  $path = dirname(__FILE__);
  require_once "$path/../npp_library.php";

  $mp3 = '';
  foreach ($items as $delta => $item) {
    if ($item['mp3']) {
      $mp3 = _npp_get_mp3($item['mp3']);
      $mp3 = check_plain($mp3);
    }
  }

  if (!$mp3) {
    return $element;
  }

  $class = check_plain($display['settings']['wordpress_player_class']);
  $height = check_plain($display['settings']['wordpress_player_height']);
  $width = check_plain($display['settings']['wordpress_player_width']);
  $location = check_plain($display['settings']['wordpress_player_location']);
  $bg = check_plain($display['settings']['wordpress_player_element_color_bg']);
  $leftbg = check_plain($display['settings']['wordpress_player_element_color_leftbg']);
  $lefticon = check_plain($display['settings']['wordpress_player_element_color_lefticon']);
  $rightbg = check_plain($display['settings']['wordpress_player_element_color_rightbg']);
  $rightbghover = check_plain($display['settings']['wordpress_player_element_color_rightbghover']);
  $righticon = check_plain($display['settings']['wordpress_player_element_color_righticon']);
  $righticonhover = check_plain($display['settings']['wordpress_player_element_color_righticonhover']);
  $text = check_plain($display['settings']['wordpress_player_element_color_text']);
  $slider = check_plain($display['settings']['wordpress_player_element_color_slider']);
  $track = check_plain($display['settings']['wordpress_player_element_color_track']);
  $border = check_plain($display['settings']['wordpress_player_element_color_border']);
  $loader = check_plain($display['settings']['wordpress_player_element_color_loader']);
  $titles = check_plain($display['settings']['wordpress_player_title_tag']);
  $downloadlink = ($display['settings']['wordpress_player_download_link']) ? TRUE : FALSE;

  $element[0]['#markup'] = theme('wordpress_player', array(
    'mp3' => $mp3,
    'class' => $class,
    'height' => $height,
    'width' => $width,
    'location' => $location,
    'bg' => $bg,
    'leftbg' => $leftbg,
    'lefticon' => $lefticon,
    'rightbg' => $rightbg,
    'rightbghover' => $rightbghover,
    'righticon' => $righticon,
    'righticonhover' => $righticonhover,
    'text' => $text,
    'slider' => $slider,
    'track' => $track,
    'border' => $border,
    'loader' => $loader,
    'titles' => $titles,
    'downloadlink' => $downloadlink,
  ));

  return $element;
}
