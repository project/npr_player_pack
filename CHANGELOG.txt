NPR Player Pack 7.x-1.1, 2013-05-10
-----------------------------------
#1986012 by musicalvegan0: Fixed with this commit:
  6565542956becb30c67dfc713f811ba2f0084a5f
  Implemented download links for all players.
#1981250 by musiclavegan0: Work around enabled
  with this commit: db0f820253234012b37c78909f14b4841d349ce7
  Will work on this issue more once usage/demand increases.
NPR Player Pack 7.x-1.0, 2013-05-03
-----------------------------------
#1980614 by musicalvegan0: Fixed with this commit:
  094d7e9bd268d4f23438059394b00874702573a2
by musicalvegan0: First release supports four players
  jPlayer
  Wordpress Player
  JW Player
  XSPF Player
